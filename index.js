const express = require("express");
const app = express();
const port = 5000;
const https = require("https");
("use strict");
const yelp = require("yelp-fusion");
// Inserire le chiavi API
const API_KEY_YELP = "Inserire API KEY di yelp";
const API_KEY_WEATHER = "Inserire API KEY del openweather";

const client = yelp.client(API_KEY_YELP);

// Esempio input di 5 città
var array_cities = ["london", "berlin", "paris", "rome", "madrid"];

// Funzione asincrona dove vengono chiamate ed integrate due API
async function getInfo(res) {
  let allInfo = [];
  let businesses = {};
  // Inizio ciclo del array_cities
  for (let i = 0; i < array_cities.length; i++) {
    let city_info = [];
    let weather_info = [];
    let bussiness_info = [];

    const location = array_cities[i];
    const appId = API_KEY_WEATHER;
    let url =
      "https://api.openweathermap.org/data/2.5/weather?q=" +
      location +
      "&appid=" +
      appId;
    // Chiamata dell'API meteo
    await https.get(url, (response2) => {
      if (response2.statusCode == 200) {
        response2.on("data", (data) => {
          weather_info = JSON.parse(data);
        });
      }
    });
    // Chiamata dell'API yelp
    await client
      .search({
        location: array_cities[i],
      })
      .then((response1) => {
        businesses = response1.jsonBody.businesses;
      })
      .catch((e) => {
        console.log(e);
      });
    // Crea array di tutti esercizi commerciali della città
    for (let index = 0; index < businesses.length; index++) {
      bussiness_info.push(businesses[index]);
    }
    // Integra le due informazioni
    city_info.push({
      weather_info: weather_info,
      businesses_info: bussiness_info,
    });
    // Push di tutte le informazioni di singola città
    allInfo.push(city_info);
  }
  // Invia una risposta JSON quando il ciclo è finito
  res.json(allInfo);
}

app.get("/", (req, res) => {
  getInfo(res);
});

app.listen(port, () => {
  //   console.log(port);
});
